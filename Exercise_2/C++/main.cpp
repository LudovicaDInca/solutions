#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <string.h>
#include <stdlib.h>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv) //argc = numero di elementi della linea di comando, numerati da zero; argv = valori degli argomenti
// in questo caso argv e' un char pointer pointer, ovvero un puntatore a puntatore e quindi un vettore
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{

    ifstream file;
    file.open(inputFilePath);

    string line;
    getline(file, line);
    text.append(line);

    // while (! file.eof()){
        // getline(getline(file, line);
        //text.append(line);}




    file.close();

    return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
   int i = 0;

    for( auto & character : text) // itera ogni carattere di text, COSTRUTTO FOR EACH
    {
      char passChar;
      passChar = password[i % password.size()];
      char code = character + passChar;
      encryptedText.append(1, code); //stringa.append(size n, n)
      i++;

    }

    return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{

    int i = 0;

    for (auto & character : text)
    {
        char passChar;
        passChar = password[i % password.size()];
        char code = character - passChar;
        decryptedText.append(1,code);
        i++;

    }

    return  true;
}
